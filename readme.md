# 项目简介
本项目是Ukulele快速框架的数据规范组件项目，前端或者服务间调用，定义了响应的消对象的规范，提供给[接口规范](https://gitee.com/timingtech/ukulele-facade)项目引用

# 快速开发框架地址
[ukulele-cloud-nacos](https://gitee.com/timingtech/ukulele-cloud-nacos)

# 分支说明
* master：主分支

# 模块说明
* Auth-Data：Auth2授权模块数据规范
* Portal-Data：系统服务数据规范
* User-Data：用户服务数据规范
* Syslog-Data：日志服务数据规范


**已经看到这里了，留个star呗**