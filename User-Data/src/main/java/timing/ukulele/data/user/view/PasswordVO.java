package timing.ukulele.data.user.view;

import lombok.Data;

@Data
public class PasswordVO {
    private String password;
    private String oldPwd;
    private String newPwd;
    private String username;
    private String email;
    private String code;
}
