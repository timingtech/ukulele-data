package timing.ukulele.data.portal.view;

import lombok.Data;

@Data
public class SysMenuPermissionVO {
    /**
     * 主键
     */
    private Long id;
    /**
     * 菜单主键
     */
    private Long menuId;
    /**
     * 索引键名称
     */
    private String permission;
    /**
     * 备注
     */
    private String remark;
    /**
     * 启警用
     */
    private Boolean deleted;
    /**
     * 是否选中
     */
    private Boolean checked;
}
