package timing.ukulele.data.portal.data;

import lombok.Data;
import lombok.EqualsAndHashCode;
import timing.ukulele.common.data.TreeNode;

import java.util.List;

/**
 * @author fengxici
 */
@EqualsAndHashCode(callSuper = true)
@Data
public class AntMenuTree extends TreeNode {
    private String text;
    private String i18n;
    private Boolean group;
    private String link;
    private Boolean linkExact;
    private String externalLink;
    private String target;
    private Integer badge;
    private Boolean badgeDot;
    private String badgeStatus;
    private Boolean disabled;
    private Boolean hide;
    private Boolean hideInBreadcrumb;
    private List<String> acl;
    private Boolean shortcut;
    private Boolean shortcutRoot;
    private Boolean reuse;
    private Boolean open;
    private String icon;
    private String fill;
    private List<String> permissionList;
    private Integer rank;
}
