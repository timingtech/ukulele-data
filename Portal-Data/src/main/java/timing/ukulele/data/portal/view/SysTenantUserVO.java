package timing.ukulele.data.portal.view;

import lombok.Data;

import java.io.Serializable;

@Data
public class SysTenantUserVO implements Serializable {
    private Long userId;
    private String username;
    private Long tenantId;
    private String tenantName;
}
