package timing.ukulele.data.portal.view;

import lombok.Data;
import lombok.EqualsAndHashCode;
import timing.ukulele.common.data.TreeNode;

@EqualsAndHashCode(callSuper = true)
@Data
public class AntMenuTreeVO extends TreeNode {
    private AntMenuVO menu;
}