package timing.ukulele.data.portal.data;

import lombok.Data;
import lombok.EqualsAndHashCode;
import timing.ukulele.common.data.TreeNode;
import timing.ukulele.data.portal.view.SysMenuPermissionVO;

import java.util.List;

@EqualsAndHashCode(callSuper = true)
@Data
public class AntMenuConfigAccessTree extends TreeNode<AntMenuConfigAccessTree> {
    Boolean checked;
    Boolean selected;
    List<SysMenuPermissionVO> permissionList;
}
