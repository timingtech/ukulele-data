package timing.ukulele.data.portal.view;

import lombok.Data;

@Data
public class SysResourceGroupVO {
    private Long id;
    private Long parentId;
    private String name;
    private String remark;
}
