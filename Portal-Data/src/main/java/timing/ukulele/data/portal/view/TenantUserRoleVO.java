package timing.ukulele.data.portal.view;

import lombok.Data;

import java.util.Set;

@Data
public class TenantUserRoleVO {
    private Long userId;
    private String username;
    private Set<Long> roleList;
    private Boolean admin;
    private Boolean owner;
}
