package timing.ukulele.data.portal.data;

import lombok.Data;
import lombok.EqualsAndHashCode;
import timing.ukulele.common.data.TreeNode;
import timing.ukulele.data.portal.view.SysResourceGroupVO;

@EqualsAndHashCode(callSuper = true)
@Data
public class SysResourceGroupTree extends TreeNode<SysResourceGroupTree> {
    private SysResourceGroupVO group;
}
