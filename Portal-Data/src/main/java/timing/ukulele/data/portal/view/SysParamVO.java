package timing.ukulele.data.portal.view;

import lombok.Data;

import java.time.LocalDateTime;

@Data
public class SysParamVO {
    private Long id;
    private String key;
    private String value;
    private String remark;
    private Integer type;
    private String typeDescription;
    private LocalDateTime createTime;
    private LocalDateTime updateTime;
    private Boolean deleted;
    private Long tenantId;
}
