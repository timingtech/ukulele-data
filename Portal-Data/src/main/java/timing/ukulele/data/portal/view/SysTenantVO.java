package timing.ukulele.data.portal.view;

import lombok.Data;

@Data
public class SysTenantVO {
    /**
     * 主键
     */
    private Long id;
    /**
     * 名称
     */
    private String name;
    /**
     * 启/禁用
     */
    private Boolean deleted;
}
