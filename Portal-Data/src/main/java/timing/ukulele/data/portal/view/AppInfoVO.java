package timing.ukulele.data.portal.view;

import lombok.Data;
import timing.ukulele.data.portal.data.AntMenuTree;

import java.util.List;
import java.util.Map;

@Data
public class AppInfoVO {
    private Map<String, Object> app;
    private Map<String, Object> user;
    private List<AntMenuTree> menu;
}
