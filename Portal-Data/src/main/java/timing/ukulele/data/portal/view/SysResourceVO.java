package timing.ukulele.data.portal.view;

import lombok.Data;

@Data
public class SysResourceVO {
    /**
     * 主键
     */
    private Long id;
    /**
     * 资源组主键
     */
    private Long groupId;
    /**
     * 索引键名称
     */
    private String resource;
    /**
     * 备注
     */
    private String remark;
    /**
     * 方法
     */
    private String method;
    /**
     * 启/禁用
     */
    private Boolean deleted;
    /**
     * 是否选中
     */
    private Boolean checked;
}
