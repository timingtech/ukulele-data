package timing.ukulele.data.portal.view;

import lombok.Data;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * 系统角色
 *
 * @author fengxici
 */
@Data
public class RoleVO implements Serializable {

    private Long id;
    private String name;
    private String code;
    private String description;
    private Integer type;
    private String typeDescription;
    private LocalDateTime createTime;
    private LocalDateTime updateTime;
    private Boolean deleted;
    private Long tenantId;
    private String tenantName;
}
